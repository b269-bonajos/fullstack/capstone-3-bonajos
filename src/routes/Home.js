import Products from '../components/Products';
import Navbar from '../components/Navbar';
import Hero from '../components/Hero';
import Items from '../components/Items';
import Footer from '../components/Footer';

function Home (){
	return(
		<>
		<Navbar />
		<Hero 
		cName="hero"
		heroImg="https://images.unsplash.com/photo-1463936575829-25148e1db1b8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1517&q=80"
		title="Green is Good"
		text="Choose your Favorite"
		buttonText="Buy Now!"
		url="/product"
		btnCLass="show"/>
		<Products/>
		<Items/>
		<Footer/>
		</>
	)
}	

export default Home;