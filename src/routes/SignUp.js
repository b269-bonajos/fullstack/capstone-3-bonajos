import Navbar from '../components/Navbar';
import Hero from '../components/Hero';
import SignUpForm from '../components/SignUpForm';
import UserContext from '../UserContext';



// function Contact (){
// 	return(
// 		<>
// 		<Navbar />
// 		<Hero 
// 		cName="hero-mid"
// 		heroImg="https://images.unsplash.com/photo-1610967911716-d1af934d4f6b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80"
// 		title="SignUp"
// 		btnCLass="hide"/>
// 		<SignUpForm/>
// 		</>
// 	)
// }	

// export default Contact;

import React, { useContext } from 'react';


export default function SignUp() {
  const { user } = useContext(UserContext);

  if (!user) {
    return <div>Loading...</div>;
  }

  return (
  <>
   		<Navbar />
 		<Hero 
		cName="hero-mid"
		heroImg="https://images.unsplash.com/photo-1610967911716-d1af934d4f6b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80"
		title="SignUp"
		btnCLass="hide"/>
 		<SignUpForm/>
		</>
  );
}

