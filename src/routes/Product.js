import Navbar from '../components/Navbar';
import Hero from '../components/Hero';
import Items from '../components/Items';


function Product() {
  return (
    <>
      <Navbar />
      <div>
        <Hero 
          cName="hero-mid"
          heroImg="https://images.unsplash.com/photo-1592150621744-aca64f48394a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1191&q=80"
          title="Product"
          btnCLass="hide"
        />
        <Items />
      </div>
    </>
  );
}

export default Product;
