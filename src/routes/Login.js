import Navbar from '../components/Navbar';
import Hero from '../components/Hero';
import LoginForm from '../components/LoginForm';


function Login (){
	return(
		<>
		 <Navbar />
		<Hero 
		cName="hero-mid"
		heroImg="https://images.unsplash.com/photo-1527241383590-5ff9a0f92f33?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
		title="Login"
		btnCLass="hide"/>
		<LoginForm/>
		</>
	)
}	

export default Login;