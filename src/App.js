
import React from 'react';
import './styles.css';
// import { Routes, Route } from 'react-router-dom';


import {useState, useEffect} from 'react';


import Home from './routes/Home';
import Login from './routes/Login';
// import Logout from './routes/Logout';
import SignUp from './routes/SignUp';
import Product from './routes/Product';
// import Error from './pages/Error';

import {Container} from 'react-bootstrap';

import {UserProvider} from './UserContext';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import './App.css'; 

function App() {

  // state hook for the user state that's defined here is for global scope
  // to store the user information and will used for validating if user is logged in on the app or not
  // const [user, setUser] = useState({email: localStorage.getItem('email')});
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

// Used to check if user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      // User is logged in
      if(typeof data._id !== "undefined") {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      // User is logged out
      else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
      
    })
  }, []);

  return (
    // <></> fragments - common pattern in React for component to return multiple elements
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
      <div className="App">
          <Routes>
              <Route path="/"element={<Home/>}/>
              <Route path="/product"element={<Product/>}/>
              <Route path="/login"element={<Login/>}/>
              <Route path="/signup"element={<SignUp/>}/>
          </Routes>
          </div>
      </UserProvider>
    </>
  );
}

export default App;


// import React from 'react';
// import './styles.css';
// import { Routes, Route } from 'react-router-dom';
// import Home from './routes/Home';
// import Login from './routes/Login';
// import Product from './routes/Product';
// import SignUp from './routes/SignUp';


// export default function App() {
//   return (
//       <div className="App">
//         <Routes>
//           <Route path="/"element={<Home/>}/>
//           <Route path="/login"element={<Login/>}/>
//           <Route path="/product"element={<Product/>}/>
//           <Route path="/signup"element={<SignUp/>}/>
//         </Routes>  
//       </div>
//   );
// }


