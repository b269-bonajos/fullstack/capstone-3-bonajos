import Product1 from "../assets/images/1.jpg"
import Product2 from "../assets/images/2.jpg"
import ProductData from "./ProductData"
import "./ProductStyles.css"

const Products = () => {
	return (
		<div className="Products">
			<h1>Sample Products</h1>
			<p>Sample Products</p>

			<ProductData
			className="first-prod"
			heading="Monstera"
			text="Monstera deliciosa, commonly called split-leaf philodendron or swiss cheese plant, is native to Central America. It is a climbing, evergreen perennial vine that is perhaps most noted for its large perforated leaves on thick plant stems and its long cord-like aerial roots"
			img1={Product1}
			img2={Product2}

			/>
			<ProductData
			className="first-prod-reverse"
			heading="Philodendron"
			text="Monstera deliciosa, commonly called split-leaf philodendron or swiss cheese plant, is native to Central America. It is a climbing, evergreen perennial vine that is perhaps most noted for its large perforated leaves on thick plant stems and its long cord-like aerial roots"
			img1={Product1}
			img2={Product2}
			
			/>
		</div>
	)
}

export default Products