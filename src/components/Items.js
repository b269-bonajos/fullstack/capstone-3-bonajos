import "./ItemsStyles.css"; 
import ItemsData from "./ItemsData"; 
import Item1 from "../assets/images/1.jpg"; 
import Item2 from "../assets/images/2.jpg"; 
import Item3 from "../assets/images/3.jpg"; 

function Items(props) {
	return (
		<div className="item">
			<h1>Price</h1>
			<p>Hot Products</p>
			<div className="itemcard">
				<ItemsData 
				image={Item1}
				heading="Monstera"
				text="Monstera deliciosa"
				url="/product"
				btnCLass="show"/>

				<ItemsData 
				image={Item2}
				heading="Monstera"
				text="Monstera deliciosa"/>
				<ItemsData 
				image={Item3}
				heading="Monstera"
				text="Monstera deliciosa"/>
				<div>
				
				</div>

			</div>
		</div>
	);
}

export default Items;

