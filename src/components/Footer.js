import "./FooterStyles.css"

const Footer = () =>{
	return(
	<div className="Footer">
		<div className="top">
			<div>
				<h1>BJNS</h1>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
			</div>
			<div>
				<a href="/">
				<i className="fa-brands fa-facebook-square"></i>
				</a>
				<a href="/">
				<i className="fa-brands fa-instagram-square"></i>
				</a>
				<a href="/">
				<i className="fa-brands fa-twitter-square"></i>
				</a>
				<a href="/">
				<i className="fa-brands fa-youtube-square"></i>
				</a>
			</div>
		</div>

		<div className="bottom">
			<div>
			<h4></h4>
			</div>
		</div>
	</div>
	)
}

export default Footer;