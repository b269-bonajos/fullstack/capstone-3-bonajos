import {Component} from "react"
import Product1 from "../assets/images/1.jpg"
import Product2 from "../assets/images/2.jpg"
import "./ProductStyles.css"

class ProductData extends Component {
	render () {
		return(
		<div className={this.props.className}>
				<div className="des-text">
					<h2>{this.props.heading}</h2>
					<p>
						{this.props.text}
					</p>
					<button>Buy Now</button>
				</div>
				<div className="image">
					<img alt="img" src={this.props.img1}/>
					<img alt="img" src={this.props.img2}/>

				</div>
			</div>
		)
	}
}

export default ProductData;


