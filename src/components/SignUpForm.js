import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';

import { Navigate, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';

import { Form, Button, input } from 'react-bootstrap';
import "./SignUpFormStyles.css"

export default function SignUpForm (){

	const{user} = useContext(UserContext)

	const navigate = useNavigate()

	const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState(false);

        useEffect(() => {
    if((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {

        setIsActive(true)
    } else {
        setIsActive(false)
    }

   }, [firstName, lastName, email, mobileNo, password1, password2])

        function registerUser(e) {

         e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if (data === true) {

                Swal.fire({
                    title: "Duplicate Email Found",
                    icon: "error",
                    text: "Kindly provide another email to complete registration."
                })
            } else {

                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if(data === true) {

                        setFirstName("");
                        setLastName("")
                        setEmail("");
                        setMobileNo("");
                        setPassword1("");
                        setPassword2("");

                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Welcome to Zuitt!"
                        })

                        navigate("/login");

                    } else {

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please, try again."
                        })
                    }

                })
            }
        })
    };

return (
  user.id !== null ? (
    <Navigate to="/signup"/>
  ) : (
    <Form onSubmit={(e) => registerUser(e)}>
      <div className="form-Container">
        <input
          type="text"
          value={firstName}
          onChange={(e) => {
            setFirstName(e.target.value);
          }}
          placeholder="Enter your First Name"
        />
        <input 
  			type="text"
  			value={lastName}
  			onChange={(e) => {setLastName(e.target.value)}}
  			placeholder="Enter your Last Name" 
  			required
			/>

        <input
          type="email"
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
          }}
          placeholder="Enter email"
        />
        <input
          type="text"
          value={mobileNo}
          onChange={(e) => {
            setMobileNo(e.target.value);
          }}
          placeholder="0999999999"
        />
        <input
          type="password"
          value={password1}
          onChange={(e) => {
            setPassword1(e.target.value);
          }}
          placeholder="Enter Your Password"
        />
        <input
          type="password"
          value={password2}
          onChange={(e) => {
            setPassword2(e.target.value);
          }}
          placeholder="Verify Your Password"
        />
        {isActive ? (
          <Button variant="primary" type="submit" id="submitBtn">
            Submit
          </Button>
        ) : (
          <Button variant="primary" type="submit" id="submitBtn" disabled>
            Submit
          </Button>
        )}
      </div>
    </Form>
  )
);
}



