export const MenuItems = [
  {
    title: "Home",
    url: "/",
    cName: "nav-links",

  },

  {
    title: "Product",
    url: "/product",
    cName: "nav-links",

  },

  {
    title: "Login",
    url: "/login",
    cName: "nav-links",
  },

  {
    title: "Sign Up",
    url: "/signup",
    cName: "nav-links",

  },
  

];
